

const balance = document.getElementById("bankBalance");
const loan = document.getElementById("outLoan");
const loanText = document.getElementById("outLoanText");
const payBalance = document.getElementById("payValue")


let currentBalance = 1000;
let currentPay = 0;
let outstandingLoan = 0;
let hasLoan = false;
let boughtComputer = false; // used to give new loan permission

updateValues(); // initializes the values

// we want the button: loan-button to give us a prompt - call the function
const loanButton = document.getElementById("loan-button");
loanButton.addEventListener("click", getLoan);

// work-button that gives us money for work (easy money)
const payButton = document.getElementById("work-button");
payButton.addEventListener("click", doWork);

// transfer-button that transfers payBalance to the balance in our bank.
const transferButton = document.getElementById("transfer-button");
transferButton.addEventListener("click", transferCash);

// repay-button that puts all pay into paying off loan
const repayButton = document.getElementById("repay-button");
repayButton.addEventListener("click", repayLoan);


// on GET LOAN button click - open prompt and take input from user
// User is denied if requested amount is too large, 
// or if they have an outstanding loan
function getLoan() {
    
    if (hasLoan){
        alert("You already have a loan! Pay it off first you ditz.");
        return;
    }

    const requested = Number(prompt("Enter loan amount"));


    // if too much is requested
    if (requested > currentBalance*2) {
        alert("You cannot borrow that much!");
        return;
    } else { // update bank balance
        hasLoan = true;
        outstandingLoan += requested;
        currentBalance += outstandingLoan;

        // show outstanding loan and repay-button
        loan.style.visibility = "visible";
        loanText.style.visibility = "visible";

        repayButton.style.visibility="visible";
        
    }
    updateValues();
} 

// Work-button which increases the "pay:" value by an amount
function doWork() {
    currentPay = currentPay + 100;
    updateValues();
}

// transfer-button that transfers from pay --> bank balance
function transferCash() {
    if (currentPay === 0) {
        alert("You want to insert 0.- into your account? you big dum");
        return;
    }

    // if user has a loan, pay a percentage towards the loan
    if (hasLoan) {
        const toBank = currentPay * 0.9;
        const toLoan = currentPay - toBank; //10% goes to pay off loan
        
        if (outstandingLoan === 0) {
            currentBalance += currentPay;
        }
        else if (outstandingLoan > 0) {
            outstandingLoan -= toLoan;
            currentBalance += toBank;
        }

    } else { // if user does not have a loan
        currentBalance += currentPay;
    }
    currentPay = 0;
    updateValues();
}

// transfers all of pay into the current loan
function repayLoan() {
    outstandingLoan -= currentPay;

    // if we have more than enough to pay the loan. Leave the surplus in pay
    if (outstandingLoan <= 0) {
        currentPay -=  (currentPay) -outstandingLoan * -1; 
        outstandingLoan = 0;
    }

    updateValues();
}

// update all the values
function updateValues() {
    balance.innerText = currentBalance.toFixed(2);
    loan.innerText = outstandingLoan.toFixed(2);
    payBalance.innerText = currentPay.toFixed(2);

    // let user take out new loans
    if (hasLoan === true && outstandingLoan === 0 && 
        boughtComputer === true) {
        hasLoan = false;
        boughtComputer = false;
    }
}


//computer section
const elDropDown = document.getElementById("selectMenu");
elDropDown.addEventListener("change", function() {
    dropdownChange();
})

let computers = []
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => computers = data)
    .then(() => logAllComputers())


// save individual computer in selection-list
function logComputer(comp) {
    //console.log(comp);
    let listItem = document.createElement("option")
    listItem.innerText = comp.title;
    elDropDown.appendChild(listItem);
}

// takes care of adding all computers from API-call to dropdown menu
function logAllComputers() {
    computers.forEach(comp => {
        logComputer(comp)
    });
}

//show computer info when user selects with the dropdown menu
const elFeatureText = document.getElementById("feature-text"); // list of features
const elCompDescription = document.getElementById("showcase-description");
const elShowcaseTitle = document.getElementById("showcase-title");
const elComputerImage = document.getElementById("computer-image");
const baseURL = "https://noroff-komputer-store-api.herokuapp.com/" // to get image
const elPrice = document.getElementById("computer-price");
function dropdownChange(){
    const compId = elDropDown.selectedIndex-1;
    const comp = computers[compId];
    const info = comp.specs;
    const description = comp.description;
    const title = comp.title; //name of computer
    const image = comp.image; //image source URL
    const price = comp.price;

    // concatenate the "facts" into a formatted string
    let newInfo = ""
    for (const fact of info) {
        // console.log(fact);
        newInfo += fact + "<br>"
    }

    elFeatureText.innerHTML = newInfo;  
    elCompDescription.innerText = description;
    elShowcaseTitle.innerText = title;
    elComputerImage.src = baseURL + image; // get image from API source
    elPrice.innerText = price + ".-";
}

// buy a computer - check bank balance - alert for fail/success
const elBuyButton = document.getElementById("buy-button");
elBuyButton.addEventListener("click", function() {
    buyComputer();
})
function buyComputer() {
    const compId = elDropDown.selectedIndex-1;
    const comp = computers[compId];
    const price = comp.price;

    if (price > currentBalance) {
        alert("You can not afford this computer!");
        return;
    }

    currentBalance -= price; // remove funds from bank
    boughtComputer = true;
    updateValues();
    alert("You are the owner of a brand 'new' laptop!")
}